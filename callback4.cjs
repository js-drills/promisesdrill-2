let fs = require("fs");
const path = require("path");
const util = require("util");

let listpath = path.join(__dirname, "./lists_1.json");
let cardspath = path.join(__dirname, "./cards_1.json");
let boardspath = path.join(__dirname, "./boards_1.json");
let usingPromises = require("./callback1.cjs");
let fetchinfListDetails = require("./callback2.cjs");
let cardsData = require("./callback3.cjs");
let readfile = util.promisify(fs.readFile);

const problem4function = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // finding board details of Thanos
      let promise1 = new Promise((resolve, reject) => {
        readfile(boardspath, "utf-8")
          .then((boarddata) => {
            try {
              let thanosId = JSON.parse(boarddata).reduce(
                (accumlator, objects) => {
                  if (objects.name === "Thanos") {
                    accumlator = objects.id;
                  }
                  return accumlator;
                },
                ""
              );
              if (thanosId) {
                Promise.allSettled([
                  usingPromises(thanosId),
                  fetchinfListDetails(thanosId),
                ])
                  .then(([board, thanoslistsdetails]) => {
                    const result = [board, thanoslistsdetails];
                    resolve(result);
                  })
                  .catch((error) => {
                    reject(error);
                  });
              }
            } catch (error) {
              reject(error);
            }
          })
          .catch((error) => {
            reject(error);
          });
      });

      // promise for finding the data of Mind
      let promise2 = new Promise((resolve, reject) => {
        readfile(listpath, "utf-8")
          .then((listdata) => {
            try {
              let listId = Object.values(JSON.parse(listdata)).reduce(
                (accumlator, currentarray) => {
                  currentarray.forEach((element) => {
                    if (element.name === "Mind") {
                      accumlator = element.id;
                    }
                  });
                  return accumlator;
                },
                ""
              );
              if (listId) {
                cardsData(listId)
                  .then((cardsdata) => {
                    resolve(cardsdata);
                  })
                  .catch((error) => {
                    reject(error);
                  });
              }
            } catch (error) {
              reject(error);
            }
          })
          .catch((error) => {
            reject(error);
          });
      });
      Promise.all([promise1, promise2])
        .then(([result1, result2]) => {
          const result = [result1, result2];
          resolve(result);
        })
        .catch((error) => {
          reject(error);
        });
    }, 5000);
  });
};

module.exports = problem4function;
