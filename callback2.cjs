
let fs = require('fs')
const path  = require("path");
const util = require('util')
let readFile = util.promisify(fs.readFile)
let fetchinfListDetails = (boardId) => {
  let listspath = path.join(__dirname, "./lists_1.json");

  return readFile(listspath, "utf-8").then((listsdata) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        try {
          if (JSON.parse(listsdata).hasOwnProperty(boardId)) {
            const parsedData = JSON.parse(listsdata);
            resolve({ boardId, data: parsedData[boardId] });
          } else {
            reject(new Error(`there's no list upon the boardId: ${boardId}`));
          }
        } catch (error) {
          reject(error);
        }
      }, 3000);
    });
  });
};
module.exports = fetchinfListDetails