let fs = require("fs");
const path = require("path");
const util = require("util");

let listpath = path.join(__dirname, "./lists_1.json");
let cardspath = path.join(__dirname, "./cards_1.json");
let boardspath = path.join(__dirname, "./boards_1.json");
let usingPromises = require("./callback1.cjs");
let fetchinfListDetails = require("./callback2.cjs");
let cardsData = require("./callback3.cjs");
let readfile = util.promisify(fs.readFile);

let problem5Function = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      readfile(boardspath, "utf-8").then((boardsData) => {
        try {
          let thanosId = JSON.parse(boardsData).find(
            (element) => element.name === "Thanos"
          ).id;
          if (thanosId) {
            Promise.all([
              usingPromises(thanosId),
              fetchinfListDetails(thanosId),
            ])
              .then(([board, listDetails]) => {
                fs.readFile(listpath, "utf-8", (error, listdata) => {
                  if (error) {
                    reject(error);
                  }
                  let listIds = Object.values(JSON.parse(listdata)).reduce(
                    (accumlator, currentarray) => {
                      currentarray.forEach((element) => {
                        if (
                          element.name === "Mind" ||
                          element.name === "Space"
                        ) {
                          accumlator.push(element.id);
                        }
                      });
                      return accumlator;
                    },
                    []
                  );
                  fs.readFile(cardspath, "utf-8", (error, cardsdata) => {
                    if (error) {
                      reject(error);
                    }
                    const cardsPromises = listIds.map((listId) => {
                      if (JSON.parse(cardsdata)[listId]) {
                        return cardsData(listId);
                      }
                    });
                    Promise.all(cardsPromises)
                      .then((cardsDataResults) => {
                        resolve([board, listDetails, cardsDataResults]);
                      })
                      .catch((error) => {
                        reject(error);
                      });
                  });
                });
              })
              .catch((error) => {
                reject(error);
              });
          }
        } catch (error) {
          reject(error);
        }
      });
    }, 5000);
  });
};


module.exports = problem5Function;
