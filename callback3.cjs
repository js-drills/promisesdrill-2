let fs = require("fs");
const path = require("path");
const util = require('util')

let readFile = util.promisify(fs.readFile)

/// fetching cardDetails using ListId 
let cardsData = (listId) => {
  let filepath = path.join(__dirname, "./cards_1.json");
  return readFile(filepath, 'utf-8').then(cardsdata => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        try {
          if (JSON.parse(cardsdata).hasOwnProperty(listId)) {
            let cards = JSON.parse(cardsdata)[listId];
            resolve({ listId,cards });
          } else {
            reject(
              new Error(`the data for the list Id ${listId} is not present`)
            );
          }
        } catch (error) {
          reject(error);
        }
      }, 4000);
    })
  })
}

module.exports= cardsData