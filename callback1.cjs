let fs = require("fs");

const path = require("path");
const util = require("util");

let readFile = util.promisify(fs.readFile);
let filepath = path.join(__dirname, "./boards_1.json");
function usingPromises(boardId) {
  return readFile(filepath, "utf-8").then((boardData) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        try {
          const board = JSON.parse(boardData).find(
            (element) => element.id === boardId
          );
          if (board) {
            resolve({ boardId, board });
          } else {
            reject(
              new Error(
                "there is no data present for the board id you have passed"
              )
            );
          }
        } catch (error) {
          reject(error);
        }
      }, 2000);
    });
  });
}
module.exports = usingPromises;
