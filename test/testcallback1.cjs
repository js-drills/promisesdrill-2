let usingPromises = require('../callback1.cjs');
let boardid ="mcu453ed"
async function fetchBoardDetails() {
  try {
    const { board, boardId } = await usingPromises(boardid);
    console.log(`The board details of Board of Id containing ${boardId}:`);
    console.log(board, "\n");
  } catch (error) {
    console.log(error);
  }
}
fetchBoardDetails()