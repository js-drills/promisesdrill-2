

let problem5Function = require("../callback5.cjs");
problem5Function()
  .then(([board, listDetails, cardsDataResults]) => {
    console.log(board, "\n\n", listDetails, "\n\n");
    cardsDataResults.forEach((card) => {
      console.log(card);
    });
  })
  .catch((error) => {
    console.log(error);
  });