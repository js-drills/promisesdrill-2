

const problem6Function = require('../callback6.cjs')
problem6Function()
  .then(([board, listdetails, listcardsdetails]) => {
    console.log(board, "\n\n", listdetails, "\nn");
    listcardsdetails.forEach((result) => {
      console.log("\n\n", result, "\n\n");
    });
  })
  .catch((error) => {
    console.log(error);
  });
