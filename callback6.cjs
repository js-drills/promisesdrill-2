let fs = require("fs");
const path = require("path");
const util = require("util");

let listpath = path.join(__dirname, "./lists_1.json");
let cardspath = path.join(__dirname, "./cards_1.json");
let boardspath = path.join(__dirname, "./boards_1.json");

let usingPromises = require("./callback1.cjs");
let fetchinfListDetails = require("./callback2.cjs");
let cardsData = require("./callback3.cjs");
let readfile = util.promisify(fs.readFile);

let problem6Function = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      readfile(boardspath, "utf-8")
        .then((boardsData) => {
          try {
            let thanosId = JSON.parse(boardsData).find(
              (element) => element.name === "Thanos"
            ).id;
            if (thanosId) {
              Promise.all([
                usingPromises(thanosId),
                fetchinfListDetails(thanosId),
              ])
                .then(([board, listdetails]) => {
                  fs.readFile(listpath, "utf-8", (error, listdata) => {
                    let listpromises = Object.values(JSON.parse(listdata))
                      .map((element) => {
                        return element.map((object) => object.id);
                      })
                      .reduce((accumlator, current) => {
                        current.forEach((value) => {
                          accumlator.push(value);
                        });
                        return accumlator;
                      }, [])
                      .reduce((accumulator, ids) => {
                        let promise = new Promise((resolve, reject) => {
                          fs.readFile(cardspath, "utf-8", (error, cardata) => {
                            if (error) {
                              reject(error); // Reject if there's an error
                              return;
                            }
                            if (JSON.parse(cardata)[ids]) {
                              resolve(cardsData(ids)); // Resolve with the result of cardsData
                            } else {
                              resolve(
                                `not macthing any card upon this Id ${ids}`
                              );
                            }
                          });
                        });
                        accumulator.push(promise);
                        return accumulator;
                      }, []);
                    Promise.all(listpromises)
                      .then((listcardsdetails) => {
                        resolve([board, listdetails, listcardsdetails]);
                      })
                      .catch((error) => {
                        reject(error);
                      });
                  });
                })
                .catch((error) => {
                  reject(error);
                });
            }
          } catch (error) {
            reject(error);
          }
        })
        .catch((error) => {
          reject(error);
        });
    }, 5000);
  });
};

module.exports = problem6Function